class Persona:

    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

    def nombreEmpleado(self):
        return self.nombre + " " + self.apellido

class Empleado(Persona):

    def __init__(self, nombre, apellido, numero):
        Persona.__init__(self,nombre, apellido)
        self.numero = numero

    def getEmpleado(self):
        return self.nombreEmpleado() + ", " +  self.numero

x = Persona("Juan", "Pérez")
y = Empleado("Lucas", "Ramirez", "1007")

print(x.nombreEmpleado())
print(y.getEmpleado())
