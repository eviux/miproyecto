class Persona:

    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

    def nombreEmpleado(self):
        return self.nombre + " " + self.apellido

class Empleado(Persona):

    def __init__(self, nombre, apellido, numero):
        super().__init__(nombre,apellido) #Con super() evitas escribir el nombre
        self.numero = numero              #de la superclase y el "self"

    def getEmpleado(self):
        return self.nombreEmpleado() + ", " +  self.numero

    def __str__(self):
        return super().nombreEmpleado() + ", " +  self.numero

x = Persona("Juan", "Pérez")
y = Empleado("Lucas", "Ramirez", "1007")

print(x.nombreEmpleado())
print(y)
