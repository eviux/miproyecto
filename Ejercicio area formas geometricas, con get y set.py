class Cuadrado:

    def __init__(self, pAltura):
        self.altura = pAltura
    def CalculoCuadrado(self):
        return "La superficie del cuadrado es: " + str(self.altura * self.altura)

    def SetAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if pAltura <= 0:
                print("Ingrese un número positivo")
            else:
                self.altura = pAltura
        else:
            print("Ingrese un número")


    def GetAltura(self):
        return "La altura del cuadrado es: " + str(self.altura)

    
Cuadrado1 = Cuadrado(5)
Cuadrado1.SetAltura(5)
print(Cuadrado1.GetAltura())
print(Cuadrado1.CalculoCuadrado())

class Triangulo:

    def __init__(self,pAltura,pBase):
        self.altura = pAltura
        self.base=pBase
    
    def CalculoTriangulo(self):
        return "La superficie del triangulo es: " +str(self.altura *self.base/2)

    def SetAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if(pAltura) <=0:
                print ("Ingrese un número positivo")
            else:
                self.altura =pAltura
        else:
            print("Ingrese un número")

    def SetBase (self, pBase):
        if type(pBase) is float or type(pBase) is int:
            if(pBase) <=0:
                print ("Ingrese un número positivo")
            else:
                self.base=pBase
        else:
            print("Ingrese un número")

    def GetAltura(self):
        return ("La altura del triangulo es: " + str(self.altura))
    def GetBase(self):
        return ("La base del triangulo es: " + str(self.base))

Triangulo1 = Triangulo(8,2) #Los números no sirven de nada
Triangulo1.SetAltura(2)
Triangulo1.SetBase(5)
print(Triangulo1.GetAltura())
print(Triangulo1.GetBase())
print(Triangulo1.CalculoTriangulo())



from math import pi

class Circulo: 
    def __init__(self, pRadio):
        self.radio=pRadio
        
    def CalculoCirculo (self):
        return (pi*(self.radio*self.radio))

    def SetRadio(self, pRadio):
        if type(pRadio) is float or type(pRadio) is int:
            if pRadio <=0:
                print("Escribe un número positivo")
            else:
                self.radio =pRadio
        else:
            print("Escribe un número")


    def GetRadio(self):
        return ("El radio del circulo es: " +str(self.radio))
    
              
Circulo1 = Circulo(8)
Circulo1.SetRadio(10.5)
print(Circulo1.GetRadio())
print("La superficie del circulo es: " + str(Circulo1.CalculoCirculo()))
